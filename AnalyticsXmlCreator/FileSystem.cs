﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace AnalyticsXmlCreator
{
    static class FileSystem
    {
        public static bool checkFileExist(string fileName)
        {
            if (File.Exists(fileName))
            {
                return true;
            }
            return false;
        }

        private static void createFile(string filePath)
        {
            try
            {
                using (StreamWriter sw = File.CreateText(filePath))
                {
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Log.write("createFile " + ex.Message.ToString());
            }
        }

        public static void createFileIfNotExist(string filePath)
        {
            if (!checkFileExist(filePath))
            {
                createFile(filePath);
            }
        }

        public static void additionFile(string filePath, string data)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(File.Open(filePath, FileMode.Append)))
                {
                    sw.WriteLine(data);
                    sw.Close();
                    sw.Dispose();
                }
            }
            catch (Exception ex)
            {
                Log.write("additionFile " + ex.Message.ToString());
            }
        }

        private static bool checkFolderExist(string folderPath)
        {
            if (Directory.Exists(folderPath))
                return true;
            return false;
        }

        private static void createFolder(string folderPath)
        {
            try
            {
                Directory.CreateDirectory(folderPath);
            }
            catch (Exception ex)
            {
                Log.write("CreateDirectory" + ex.Message.ToString());
            }
        }

        public static void createFolderIfNotExist(string folderPath)
        {
            if (!checkFolderExist(folderPath))
            {
                createFolder(folderPath);
            }
        }
        
    }
}

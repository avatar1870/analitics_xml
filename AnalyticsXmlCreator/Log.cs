﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AnalyticsXmlCreator
{
    public static class Log
    {
        private static string projectPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));

        private static string folderPath = "Log";
        private static string filePath = "log.txt";

        private static string fullFilePath = projectPath + "/" + folderPath + "/" + filePath;
        private static string fullFolderPath = projectPath + "/" + folderPath;

        public static void validateFiles()
        {
            FileSystem.createFolderIfNotExist(fullFolderPath);
            FileSystem.createFileIfNotExist(fullFilePath);
        }

        public static void write(string str)
        {
            string fullString = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " - " + str;
            FileSystem.additionFile(fullFilePath, fullString);
        }
    }
}

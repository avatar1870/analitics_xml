﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnalyticsXmlCreator
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Log.validateFiles();
            Log.write("Начало работы");

            string processParentName = Process.GetProcessById(Process.GetCurrentProcess().Id).Parent().ProcessName;

            if (processParentName == "cmd")
            {
                Launcher.useCmd = true;
                Launcher.validateParams(args);
            }
            else
            {
                Log.write("Обычный запуск" + "\n");

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1()); 
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Web;
using System.Windows.Forms;

namespace AnalyticsXmlCreator
{
    class XMLUpdater
    {
        public delegate void LogTextDelegate(string text, int count = 0);
        public event LogTextDelegate onHtml;
        public event LogTextDelegate onEvents;
        public event LogTextDelegate onParams;
        public event LogTextDelegate onError;

        private class AwemEvent
        {
            public string name;
            public bool onWiki;
            public Dictionary<string, AwemParam> paramDict = new Dictionary<string, AwemParam>();
        }

        // параметр в таблице ивента (wiki.html)
        // http://mogilev.awem.by/mediawiki/index.php/AwemAnalytics_2.6.0 или новее
        // пример параметра из analytics_events.xml (тег Param внутри тега Event)
        // <Event title = "AbnormalRealtimeCorrection" >
        //     < Common >
        //           < Param title="GameSessionId"     required="0" disabled="1" />
        private class AwemParam
        {
            public string group;        // название группы параметров ( в html обычно ячейка на всю ширину таблицы, текст внутри жирным шрифтом)
            public string name;         // имя парамера из таблицы (1-й столбик)
            public string description;  // описание параметра (2-й столбик)
            public string example;      // пример значения параметра (3-й столбик)
            public bool required;       // является ли обязательным (4-й столбик) ( true только если "Обязательное", без скобок)
            public bool common;         // является ли общим для всех ( берется из старого xml)
            public bool disabled;       // отключен ли (берется из старого xml)
            public bool onWiki;         // true если присутствует на wiki
        }

        private class AwemParamInfo
        {
            public string name;
            public string regex;
            public string eventName;
            public string example;
            public string description;
            public bool common;
            public bool onWiki; 
        }

        private Dictionary<string, AwemEvent> eventDict = new Dictionary<string, AwemEvent>();
        private Dictionary<string, AwemParamInfo> paramDict = new Dictionary<string, AwemParamInfo>();

        private string xmlEventsOpening;

        public bool tryLoadWiki(string filepath)
        {
            string html = null;

            try
            {
                StreamReader sr = new StreamReader(filepath);
                html = sr.ReadToEnd();
                sr.Close();

                parseHTML(html);

                if (!Launcher.useCmd)
                    onHtml(html, 0);

                string eventList = "";
                foreach (var it in eventDict.OrderBy(x => x.Key))
                    eventList += string.Format("{0} \n", it.Key);

                if (!Launcher.useCmd)
                    onEvents(eventList, eventDict.Count);

                string paramList = "";
                foreach (var it in paramDict.OrderBy(x => x.Key))
                    paramList += string.Format("{0} \n", it.Key);

                if (!Launcher.useCmd)
                    onParams(paramList, paramDict.Count);
            }
            catch (Exception e)
            {
                if(!Launcher.useCmd)
                    onError(e.ToString());

                return false;
            }

            return true;
        }

        public bool tryLoadEvents(string filepath)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filepath);
                parseXmlEvents(xmlDoc);

                string docStr = File.ReadAllText(filepath);
                xmlEventsOpening = docStr.Substring(0, docStr.IndexOf("<Event"));

                string eventList = "";
                foreach (var it in eventDict.OrderBy(x => x.Key))
                    eventList += string.Format("{0} \n", it.Key);
                onEvents(eventList, eventDict.Count);

                string paramList = "";
                foreach (var it in paramDict.OrderBy(x => x.Key))
                    paramList += string.Format("{0} \n", it.Key);
                onParams(paramList, paramDict.Count);
            }
            catch (Exception e)
            {
                onError(e.ToString());
                return false;
            }

            return true;
        }

        public bool tryLoadParams(string filepath)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filepath);
                parseXmlParams(xmlDoc);

                string paramList = "";
                foreach (var it in paramDict.OrderBy(x => x.Key))
                    paramList += string.Format("{0} \n", it.Key);
                onParams(paramList, paramDict.Count);
            }
            catch (Exception e)
            {
                onError(e.ToString());
                return false;
            }

            return true;
        }


        private void updateParam(string eventname, AwemParam param, bool fromWiki)
        {
            if (string.IsNullOrEmpty(eventname)) return;

            AwemEvent current_event = null;
            if (eventDict.ContainsKey(eventname))
                current_event = eventDict[eventname];
            else
            {
                current_event = new AwemEvent();
                eventDict.Add(eventname, current_event);
            }
            current_event.name = eventname;
            if (fromWiki) current_event.onWiki = true;

            if (param == null || string.IsNullOrEmpty(param.name)) return;

            AwemParam current_param = null;
            if (current_event.paramDict.ContainsKey(param.name))
                current_param = current_event.paramDict[param.name];
            else
            {
                current_param = new AwemParam();
                current_event.paramDict.Add(param.name, current_param);
            }


            // validate fields
            if (!string.IsNullOrEmpty(param.group)) current_param.group = param.group;
            if (!string.IsNullOrEmpty(param.name)) current_param.name = param.name;
            if (!string.IsNullOrEmpty(param.description)) current_param.description = param.description;
            if (!string.IsNullOrEmpty(param.example)) current_param.example = param.example;
            if (fromWiki)
            {
                current_param.required = param.required;
                current_param.disabled = false;
            }
            if (!fromWiki) current_param.disabled = param.disabled;
            if (!fromWiki) current_param.common = param.common;
            if (fromWiki) current_param.onWiki = true;

            AwemParamInfo paramInfo = new AwemParamInfo();
            paramInfo.name = current_param.name;
            paramInfo.description = current_param.description;
            paramInfo.example = current_param.example;
            paramInfo.common = current_param.common;
            paramInfo.regex = "";
            paramInfo.eventName = eventname;
            paramInfo.onWiki = current_param.onWiki;
            updateParamInfo(paramInfo);
        }

        private void updateParamInfo(AwemParamInfo paramInfo)
        {
            if (string.IsNullOrEmpty(paramInfo.name)) return;

            AwemParamInfo current_info = null;
            if (paramDict.ContainsKey(paramInfo.name))
                current_info = paramDict[paramInfo.name];
            else
            {
                current_info = new AwemParamInfo();
                paramDict.Add(paramInfo.name, current_info);
            }

            // validate fields
            if (!string.IsNullOrEmpty(paramInfo.name)) current_info.name = paramInfo.name;
            if (!string.IsNullOrEmpty(paramInfo.regex) && string.IsNullOrEmpty(current_info.regex)) current_info.regex = paramInfo.regex;
            if (!string.IsNullOrEmpty(paramInfo.eventName)) current_info.eventName = paramInfo.eventName;
            if (!string.IsNullOrEmpty(paramInfo.example)) current_info.example = paramInfo.example;
            if (!string.IsNullOrEmpty(paramInfo.description)) current_info.description = paramInfo.description;
            if (paramInfo.common) current_info.common = true;
            if (paramInfo.onWiki) current_info.onWiki = true;
    }

        private string replaceTags(string html)
        {
            string result = html;
            int k = result.IndexOf("<");
            if (k < 0) return html;
            while (k >= 0)
            {
                int k2 = result.IndexOf(">", k);
                if (k2 >= 0)
                    result = result.Substring(0, k).Trim() + "|" + result.Substring(k2 + 1).Trim();
                k = result.IndexOf("<");
            }
            result = result.Replace("||", "|");
            if (result[result.Length - 1] == '|')
                result = result.Substring(0, result.Length - 1);
            return result;
        }
        private void parseHTML(string html)
        {
            string active_group_name = "";
            bool skippedFirst = false;

            var dom = CsQuery.CQ.CreateDocument(html);
            dom["table"].Each((i, e) =>
            {
                if (e.GetAttribute("border") != null)
                {
                    if (!skippedFirst)
                    {
                        skippedFirst = true;
                    }
                    else
                    {
                        string tableName = "";


                        if (e.PreviousElementSibling.NodeName == "H3")
                            tableName = e.PreviousElementSibling.LastElementChild.InnerText.Trim();
                        else if (e.PreviousElementSibling.PreviousElementSibling.NodeName == "H3")
                            tableName = e.PreviousElementSibling.PreviousElementSibling.LastElementChild.InnerText.Trim();

                        var html_table = CsQuery.CQ.Create(e.OuterHTML);
                        html_table["tr"].Each((ii, row) =>
                        {
                            if (ii != 0)
                            {
                                if (row.ChildNodes.Count == 1)
                                    active_group_name = row.FirstChild.InnerText;
                                else
                                {
                                    var tds = row.ChildElements.ToArray();

                                    // обычная таблица, описывающая один ивент
                                    if (tds.Length >= 5)
                                    {
                                        string paramName = tds[0].InnerText.Trim();
                                        if (!string.IsNullOrEmpty(paramName))
                                        {
                                            AwemParam parameter = new AwemParam();
                                            parameter.group = active_group_name;
                                            parameter.name = System.Net.WebUtility.HtmlDecode(paramName.Trim());
                                            parameter.description = System.Net.WebUtility.HtmlDecode(tds[1].InnerText.Trim());
                                            parameter.example = System.Net.WebUtility.HtmlDecode(replaceTags(tds[2].InnerHTML).Trim());
                                            parameter.required = System.Net.WebUtility.HtmlDecode(tds[3].InnerText.Trim()).Trim() == "Обязательное" ? true : false;
                                            parameter.disabled = false;
                                            parameter.common = false;

                                            updateParam(tableName, parameter, true);
                                        }
                                    }
                                    // таблица правил regex для параметров
                                    else if (tds.Length == 3 && tableName == "Parameters_Regular_Expressions")
                                    {
                                        AwemParamInfo paramInfo = new AwemParamInfo();
                                        paramInfo.name = tds[0].InnerText.Trim();
                                        paramInfo.example = System.Net.WebUtility.HtmlDecode(tds[1].InnerText.Trim());
                                        paramInfo.regex = tds[2].InnerText.Trim();
                                        updateParamInfo(paramInfo);
                                    } 
                                }
                            }
                        });
                    }
                }
            });

            // ищем ивенты без таблиц (без параметров)

            Regex r = new Regex("^[0-9A-Za-z_]+$");
            dom["h3 span.mw-headline"].Each((i, e) => 
            {
                string eventName = e.InnerText.Trim();
                if (r.IsMatch(eventName) && eventName != "Parameters_Regular_Expressions")
                {
                    updateParam(eventName, null, true);
                }
            });
        }

        private void parseXmlEvents(XmlDocument doc)
        {
            XmlNode xml = doc.SelectSingleNode("AwemAnalytics");

            foreach (XmlNode node in xml.SelectNodes("Event"))
            {
                string tableName = node.Attributes["title"].Value;

                foreach (XmlNode commonParam in node.SelectNodes("Common")[0].SelectNodes("Param"))
                {
                    AwemParam parameter = new AwemParam();
                    parameter.name = commonParam.Attributes["title"].Value;
                    parameter.required = commonParam.Attributes["required"].Value == "1";
                    parameter.disabled = true;
                    parameter.common = true;

                    updateParam(tableName, parameter, false);
                }

                foreach (XmlNode commonParam in node.SelectNodes("Specific")[0].SelectNodes("Param"))
                {
                    AwemParam parameter = new AwemParam();
                    parameter.name = commonParam.Attributes["title"].Value;
                    parameter.required = commonParam.Attributes["required"].Value == "1";
                    parameter.disabled = commonParam.Attributes["disabled"] != null;
                    parameter.common = false;

                    updateParam(tableName, parameter, false);
                }
            }
        }

        private void parseXmlParams(XmlDocument doc)
        {
            XmlNode xml = doc.SelectSingleNode("AwemAnalytics");

            foreach (XmlNode node in xml.SelectNodes("Param"))
            {
                AwemParamInfo paramInfo = new AwemParamInfo();
                paramInfo.name = node.Attributes["title"].Value;
                paramInfo.regex = node.Attributes["regex"].Value;
                updateParamInfo(paramInfo);
            }
        }



        public void saveNewXml(string folder, bool onlywiki)
        {
            if (eventDict.Count == 0 && paramDict.Count == 0) return;

            File.WriteAllText(folder + "/analytics_events.xml", xmlEventsOpening + GetEventsXML(onlywiki) + "\n</AwemAnalytics>");
            File.WriteAllText(folder + "/analytics_params.xml", "<?xml version=\"1.0\"?>\n<AwemAnalytics>" + GetParamsXML(onlywiki) + "\n</AwemAnalytics>");

            Log.write("Файлы сохранены успешно");
        }

        private string GetEventsXML(bool onlywiki)
        {
            var commonParamList = paramDict.Where(x => x.Value.common == true).OrderBy(x => x.Key);

            List<string> res = new List<string>();
            //res.Add("<Events>");
            foreach (var table in eventDict.OrderBy(x => x.Key))
            {
                if (onlywiki && !table.Value.onWiki) continue;

                res.Add(string.Format("\t<Event title=\"{0}\">", table.Key));
                res.Add("\t\t<Common>");

                foreach (var p in commonParamList)
                {
                    if (table.Value.paramDict.ContainsKey(p.Key))
                    {
                        var param = table.Value.paramDict[p.Key];
                        res.Add(string.Format("\t\t\t<Param title=\"{0,-30} required=\"{1}\" {2}/>",
                            param.name + "\"",
                            param.required ? 1 : 0,
                            param.disabled ? "disabled=\"1\" " : ""));
                    }
                    else
                    {
                        res.Add(string.Format("\t\t\t<Param title=\"{0,-30} required=\"{1}\" {2}/>",
                            p.Value.name + "\"",
                            0,
                           "disabled=\"1\" "));
                    }
                }

                res.Add("\t\t</Common>");
                res.Add("\t\t<Specific>");
                foreach (var param in table.Value.paramDict.OrderBy(x => x.Key))
                {
                    if (onlywiki && !param.Value.onWiki) continue;
                    if (param.Value.common == false && paramDict[param.Key].common == false)
                        res.Add(string.Format("\t\t\t<Param title=\"{0,-30} required=\"{1}\" {2}/>",
                            param.Key + "\"",
                            param.Value.required ? 1 : 0,
                            param.Value.disabled ? "disabled=\"1\" " : ""));
                }
                res.Add("\t\t</Specific>");
                res.Add("\t</Event>");
            }
            //res.Add("</Events>");

            return String.Join("\n", res);
        }

        private string GetParamsXML(bool onlywiki)
        {
            List<string> res = new List<string>();
            //res.Add("<Params>");
            foreach (var param in paramDict.OrderBy(x => x.Key))
            {
                if (onlywiki && !param.Value.onWiki) continue;

                res.Add(string.Format("\t<Param title=\"{0,-30} regex=\"{4}\" />\n\t<!-- event example = ({1})-->\n\t<!-- example descr = ({2}) -->\n\t<!-- example value = ({3})-->\n",
                    param.Key + "\"", 
                    param.Value.eventName, 
                    param.Value.description, 
                    param.Value.example, 
                    string.IsNullOrEmpty(param.Value.regex) 
                        ? "" : param.Value.regex));
            }
            //res.Add("</Params>");

            return String.Join("\n", res);
        }

        public string GetWikiRegexTable()
        {
            List<string> res = new List<string>();

            res.Add("");
            res.Add("=== Parameters_Regular_Expressions ===");
            res.Add("");
            res.Add("правила для значений в параметрах в виде regex");
            res.Add("");
            res.Add("{| border=\"1\" cellspacing=\"1\" cellpadding=\"1\" style=\"width: 1000px; \"");
            res.Add("|-");
            res.Add("| Параметр");
            res.Add("| Пример значения");
            res.Add("| Regular Expression");
            res.Add("|-");

            foreach (var p in paramDict.OrderBy(x => x.Key))
            {
                res.Add("| " + p.Key);
                res.Add("| " + (string.IsNullOrEmpty(p.Value.example) 
                    ? "" : p.Value.example.Replace("|", "<br/>")));
                res.Add("| " + (string.IsNullOrEmpty(p.Value.regex) 
                    ? "" : p.Value.regex.Replace("|", "&#124;")));
                res.Add("|-");
            }
            res.RemoveAt(res.Count - 1);

            res.Add("|}");
            res.Add("");

            return String.Join("\n", res);
        }

        private bool isRegexValid(string regex)
        {
            if (string.IsNullOrEmpty(regex)) return true;

            try
            {
                new Regex(regex);
                return true;
            }
            catch { }

            return false;
        }

        public void validate()
        {
            Regex name = new Regex("^[0-9A-Za-z_]+$");
            foreach (var i in eventDict)
            {
                if (!i.Value.onWiki) onError(string.Format("event ({0}) not on wiki", i.Key));
                if (!name.IsMatch(i.Key)) onError(string.Format("{0} event ({1}) has invalid name", i.Value.onWiki ? "wiki" : "xml", i.Key));
                foreach (var j in i.Value.paramDict)
                {
                    if (!j.Value.common && !j.Value.onWiki) onError(string.Format("param ({0}) in event ({1}) not on wiki", j.Key, i.Key));
                    if (!name.IsMatch(j.Key)) onError(string.Format("{0} param ({1}) in event ({2}) has invalid name", j.Value.onWiki ? "wiki" : "xml", j.Key, i.Key));
                }
            }
            foreach (var p in paramDict)
            {
                if (string.IsNullOrEmpty(p.Value.regex)) onError(string.Format("param ({0}) in event ({1}) has no regex", p.Key, p.Value.eventName));
                else if (!isRegexValid(p.Value.regex)) onError(string.Format("param ({0}) in event ({1}) has invalid regex ({2})", p.Key, p.Value.eventName, p.Value.regex));
            }
        }

        public void reset()
        {
            eventDict.Clear();
            paramDict.Clear();
        }
    }
}

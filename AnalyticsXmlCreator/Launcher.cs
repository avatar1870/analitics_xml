﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace AnalyticsXmlCreator
{
    public static class Launcher
    {
        public static bool useCmd = false;

        public static void validateParams(string[] args)
        {
            int argsCount = args.Count();           

            switch(argsCount)
            {
                case 0:
                    {
                        Log.write("Запуск из командной строки без параметров");
                        Start("", ".");                        
                        break;
                    }

                case 1:
                    {
                        Log.write("Запуск из командной строки с путем до xml: "  + args[0]);
                        Start(args[0], ".");                        
                        break;
                    }

                case 2:
                    {
                        Log.write("Запуск из командной строки с путем до xml: " + args[0] + " и до сохранения xml: " + args[1]);
                        Start(args[0], args[1]);                        
                        break;
                    }

                default:
                    {
                        Log.write("Неправильное число параметров: " + argsCount);
                        Application.Exit();
                        break;
                    }
            }
           
        }

        private static void Start(string htmlPath, string saveXmlPath)
        {
            try
            {
                XMLUpdater xmlUpdater = new XMLUpdater();
                xmlUpdater.onError += writeErrorLog;                

                if (string.IsNullOrEmpty(htmlPath))
                {
                    if (FileSystem.checkFileExist("./wiki.html"))
                    {
                        Log.write("Существует исходный html файл по пути: " + "./wiki.html");
                        xmlUpdater.tryLoadWiki("./wiki.html");
                    }
                    else
                    {
                        Log.write("Не существует исходный html файл по пути: " + "../../wiki.html");
                    }
                }
                else
                {
                    if (FileSystem.checkFileExist(htmlPath))
                    {
                        Log.write("Существует исходный html файл по пути: " + htmlPath);
                        xmlUpdater.tryLoadWiki(htmlPath);                       
                    }
                    else
                    {
                        Log.write("Не существует исходный html файл по пути: " + htmlPath);
                    }
                }

                if (string.IsNullOrEmpty(saveXmlPath))
                    saveXmlPath = "./";

                xmlUpdater.validate();
                xmlUpdater.saveNewXml(saveXmlPath, true);
            }
            catch (Exception ex)
            {
                Log.write("Launcher.Start: " + ex.Message.ToString());
            }
            finally
            {
                Log.write("Завершение работы " + Environment.NewLine + Environment.NewLine);
            }
        }

        private static void writeErrorLog(string text, int count)
        {
            string str = text;
            text += "Error (" + count + ")";

            Log.write(str);
        }
    }
}

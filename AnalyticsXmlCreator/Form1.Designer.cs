﻿namespace AnalyticsXmlCreator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.listEvents = new System.Windows.Forms.RichTextBox();
            this.listParams = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button4 = new System.Windows.Forms.Button();
            this.errors = new System.Windows.Forms.RichTextBox();
            this.btParams = new System.Windows.Forms.Button();
            this.isOnlyWiki = new System.Windows.Forms.CheckBox();
            this.lbEvents = new System.Windows.Forms.Label();
            this.lbParams = new System.Windows.Forms.Label();
            this.lbErrors = new System.Windows.Forms.Label();
            this.lbWiki = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btReset = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.btClearErrors = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(231, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Open wiki.html";
            this.toolTip1.SetToolTip(this.button1, "open https://mogilev.awem.by:19080/mediawiki/index.php/AwemAnalytics_2.6.0 and sa" +
        "ve as wiki.html");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btWiki);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 41);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(676, 134);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // listEvents
            // 
            this.listEvents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listEvents.Location = new System.Drawing.Point(12, 181);
            this.listEvents.Name = "listEvents";
            this.listEvents.Size = new System.Drawing.Size(246, 287);
            this.listEvents.TabIndex = 3;
            this.listEvents.Text = "";
            // 
            // listParams
            // 
            this.listParams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listParams.Location = new System.Drawing.Point(264, 181);
            this.listParams.Name = "listParams";
            this.listParams.Size = new System.Drawing.Size(423, 162);
            this.listParams.TabIndex = 4;
            this.listParams.Text = "";
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Location = new System.Drawing.Point(15, 474);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "save xml";
            this.toolTip1.SetToolTip(this.button3, "saves files to current folder");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(329, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(153, 23);
            this.button4.TabIndex = 7;
            this.button4.Text = "Open analytics_events.xml";
            this.toolTip1.SetToolTip(this.button4, "open analytics_events.xml from game/bin/settings");
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.btEvents);
            // 
            // errors
            // 
            this.errors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errors.Location = new System.Drawing.Point(264, 349);
            this.errors.Name = "errors";
            this.errors.Size = new System.Drawing.Size(423, 177);
            this.errors.TabIndex = 8;
            this.errors.Text = "";
            this.toolTip1.SetToolTip(this.errors, "these program will fix more errors automatically, except regex errors");
            // 
            // btParams
            // 
            this.btParams.Location = new System.Drawing.Point(488, 12);
            this.btParams.Name = "btParams";
            this.btParams.Size = new System.Drawing.Size(158, 23);
            this.btParams.TabIndex = 9;
            this.btParams.Text = "Open analytics_params.xml";
            this.toolTip1.SetToolTip(this.btParams, "open analytics_params.xml from game/bin/settings");
            this.btParams.UseVisualStyleBackColor = true;
            this.btParams.Click += new System.EventHandler(this.btParams_Click);
            // 
            // isOnlyWiki
            // 
            this.isOnlyWiki.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.isOnlyWiki.AutoSize = true;
            this.isOnlyWiki.Checked = true;
            this.isOnlyWiki.CheckState = System.Windows.Forms.CheckState.Checked;
            this.isOnlyWiki.Location = new System.Drawing.Point(136, 478);
            this.isOnlyWiki.Name = "isOnlyWiki";
            this.isOnlyWiki.Size = new System.Drawing.Size(89, 17);
            this.isOnlyWiki.TabIndex = 10;
            this.isOnlyWiki.Text = "only from wiki\r\n";
            this.toolTip1.SetToolTip(this.isOnlyWiki, "default = true (false only for debug)\r\nif xml contains events,which are not on wi" +
        "ki,\r\nyou should add event on wiki or remove it from xml ");
            this.isOnlyWiki.UseVisualStyleBackColor = true;
            // 
            // lbEvents
            // 
            this.lbEvents.AutoSize = true;
            this.lbEvents.Location = new System.Drawing.Point(194, 184);
            this.lbEvents.Name = "lbEvents";
            this.lbEvents.Size = new System.Drawing.Size(50, 13);
            this.lbEvents.TabIndex = 11;
            this.lbEvents.Text = "EVENTS";
            // 
            // lbParams
            // 
            this.lbParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbParams.AutoSize = true;
            this.lbParams.Location = new System.Drawing.Point(620, 184);
            this.lbParams.Name = "lbParams";
            this.lbParams.Size = new System.Drawing.Size(52, 13);
            this.lbParams.TabIndex = 12;
            this.lbParams.Text = "PARAMS";
            // 
            // lbErrors
            // 
            this.lbErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbErrors.AutoSize = true;
            this.lbErrors.ForeColor = System.Drawing.Color.Red;
            this.lbErrors.Location = new System.Drawing.Point(624, 355);
            this.lbErrors.Name = "lbErrors";
            this.lbErrors.Size = new System.Drawing.Size(53, 13);
            this.lbErrors.TabIndex = 13;
            this.lbErrors.Text = "ERRORS";
            // 
            // lbWiki
            // 
            this.lbWiki.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbWiki.AutoSize = true;
            this.lbWiki.Location = new System.Drawing.Point(601, 51);
            this.lbWiki.Name = "lbWiki";
            this.lbWiki.Size = new System.Drawing.Size(64, 13);
            this.lbWiki.TabIndex = 14;
            this.lbWiki.Text = "WIKI HTML";
            // 
            // btReset
            // 
            this.btReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btReset.Location = new System.Drawing.Point(645, 12);
            this.btReset.Name = "btReset";
            this.btReset.Size = new System.Drawing.Size(43, 23);
            this.btReset.TabIndex = 18;
            this.btReset.Text = "reset";
            this.toolTip1.SetToolTip(this.btReset, "need for reset loaded data (every opening file loads data into memory and don\'t c" +
        "lear it until exit)");
            this.btReset.UseVisualStyleBackColor = true;
            this.btReset.Click += new System.EventHandler(this.btReset_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(15, 503);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "save xml as ...";
            this.toolTip1.SetToolTip(this.button2, "saves data to selected folder");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(528, 152);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(159, 23);
            this.button5.TabIndex = 20;
            this.button5.Text = "Copy Regex Table for wiki";
            this.toolTip1.SetToolTip(this.button5, "only for filling regex table on wiki from old xml");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btClearErrors
            // 
            this.btClearErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClearErrors.Location = new System.Drawing.Point(627, 503);
            this.btClearErrors.Name = "btClearErrors";
            this.btClearErrors.Size = new System.Drawing.Size(46, 23);
            this.btClearErrors.TabIndex = 15;
            this.btClearErrors.Text = "clear";
            this.btClearErrors.UseVisualStyleBackColor = true;
            this.btClearErrors.Click += new System.EventHandler(this.btClearErrors_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 499);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 26);
            this.label1.TabIndex = 16;
            this.label1.Text = "(non wiki events and params \r\nwill be removed from xml)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(12, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "please open all three files for minimum errors";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 534);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btReset);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btClearErrors);
            this.Controls.Add(this.lbWiki);
            this.Controls.Add(this.lbErrors);
            this.Controls.Add(this.lbParams);
            this.Controls.Add(this.lbEvents);
            this.Controls.Add(this.isOnlyWiki);
            this.Controls.Add(this.btParams);
            this.Controls.Add(this.errors);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.listParams);
            this.Controls.Add(this.listEvents);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Awem Analytics XML updater";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox listEvents;
        private System.Windows.Forms.RichTextBox listParams;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.RichTextBox errors;
        private System.Windows.Forms.Button btParams;
        private System.Windows.Forms.CheckBox isOnlyWiki;
        private System.Windows.Forms.Label lbEvents;
        private System.Windows.Forms.Label lbParams;
        private System.Windows.Forms.Label lbErrors;
        private System.Windows.Forms.Label lbWiki;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Button btClearErrors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btReset;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ToolTip toolTip3;
    }
}


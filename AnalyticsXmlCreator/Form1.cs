﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace AnalyticsXmlCreator
{
    public partial class Form1 : Form
    {
        XMLUpdater xmlUpdater = null;

        public Form1()
        {
            InitializeComponent();

            xmlUpdater = new XMLUpdater();

            if (!Launcher.useCmd)
            {
                xmlUpdater.onHtml += on_HTML;
                xmlUpdater.onEvents += on_Events;
                xmlUpdater.onParams += on_Params;
                xmlUpdater.onError += on_Error;
            }
        }

        private void btWiki(object sender, EventArgs e)
        {
            if (File.Exists("./wiki.html"))
                xmlUpdater.tryLoadWiki("./wiki.html");
            else
            {
                openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    xmlUpdater.tryLoadWiki(openFileDialog1.FileName);
            }
            errors.Clear();
            xmlUpdater.validate();
        }

        private void btEvents(object sender, EventArgs e)
        {
            if (File.Exists("./analytics_events.xml"))
                xmlUpdater.tryLoadEvents("./analytics_events.xml");
            else
            {
                openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    xmlUpdater.tryLoadEvents(openFileDialog1.FileName);
            }
            errors.Clear();
            xmlUpdater.validate();
        }

        private void btParams_Click(object sender, EventArgs e)
        {
            if (File.Exists("./analytics_params.xml"))
                xmlUpdater.tryLoadParams("./analytics_params.xml");
            else
            {
                openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    xmlUpdater.tryLoadParams(openFileDialog1.FileName);
            }
            errors.Clear();
            xmlUpdater.validate();
        }


        public void on_HTML(string text, int count)
        {
            richTextBox1.Text = text;
        }

        public void on_Events(string text, int count)
        {
            listEvents.Text = text;
            lbEvents.Text = "EVENTS (" + count + ")";
        }

        public void on_Params(string text, int count)
        {
            listParams.Text = text;
            lbParams.Text = "PARAMS (" + count + ")";
        }

        public void on_Error(string text, int count)
        {
            errors.Text += text + "\n";
        }

        private void btClearErrors_Click(object sender, EventArgs e)
        {
            errors.Clear();
        }

        private void btReset_Click(object sender, EventArgs e)
        {
            xmlUpdater.reset();
            richTextBox1.Clear();
            listEvents.Clear();
            listParams.Clear();
            errors.Clear();
            lbErrors.Text = "ERRORS";
            lbParams.Text = "PARAMS";
            lbEvents.Text = "EVENTS";
        }



        private void button3_Click(object sender, EventArgs e)
        {
            xmlUpdater.saveNewXml("./", isOnlyWiki.Checked);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                xmlUpdater.saveNewXml(folderBrowserDialog1.SelectedPath, isOnlyWiki.Checked);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string regexTable = xmlUpdater.GetWikiRegexTable();
            Clipboard.SetText(regexTable);
            MessageBox.Show("was copied into buffer (ctrl+c), use ctrl+v for pasting");
        }
    }
}
